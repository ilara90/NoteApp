﻿using Microsoft.EntityFrameworkCore;
using NotesApp.WebApi.Models.DomainModels;

namespace NotesApp.WebApi.Data
{
    public class NotesDbContext : DbContext
    {
        public NotesDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
