﻿using NotesApp.WebApi.Models.DTO;

namespace NotesApp.WebApi.Models.DomainModels
{
    public class Note
    {
        public Guid Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? ColorHex { get; set; }
        public DateTime DateCreated { get; set; }
        public Category Category { get; set; }
        public Guid? CategoryId { get; set; }
    }
}
