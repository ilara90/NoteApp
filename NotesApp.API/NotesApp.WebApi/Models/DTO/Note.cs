﻿using NotesApp.WebApi.Models.DomainModels;

namespace NotesApp.WebApi.Models.DTO
{
    public class Note
    {
        public Guid Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? ColorHex { get; set; }
        public DateTime DateCreated { get; set; }
        public Guid? CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
